DROP DATABASE IF EXISTS Convention;

CREATE DATABASE Convention;

USE Convention;

CREATE TABLE Azienda(
    RagSocAz VARCHAR(30) NOT NULL,
    IndAz VARCHAR(30) NULL,
    TelAz VARCHAR(15) NULL,
    PRIMARY KEY (RagSocAz)
);

CREATE TABLE Utente(
    IDUtente INT NOT NULL AUTO_INCREMENT,
    Psw char(64) NULL,
    Username VARCHAR(10) UNIQUE,
    Mail VARCHAR(30) NULL,
    Nome VARCHAR(20) NULL,
    Cognome VARCHAR(20) NULL,
    PRIMARY KEY(IDUtente)
);

CREATE TABLE Relatore(
    IDRel INT NOT NULL,
    RagSocAz VARCHAR(30) NULL,
    FOREIGN KEY(RagSocAz) REFERENCES Azienda(RagSocAz),
    FOREIGN KEY(IDRel) REFERENCES Utente(IDUtente),
    PRIMARY KEY (IDRel) 
);

CREATE TABLE Speech(
    Titolo VARCHAR(20) NOT NULL,
    Argomento VARCHAR(40) NULL,
    PRIMARY KEY (Titolo)
);

CREATE TABLE Piano(
    Numero INT NOT NULL AUTO_INCREMENT,
    PRIMARY KEY (Numero)
);

CREATE TABLE Sala(
    NomeSala VARCHAR(20) NOT NULL,
    NpostiSala INT NULL,
    Numero INT NULL,
    FOREIGN KEY (Numero) REFERENCES Piano(Numero),
    PRIMARY KEY (NomeSala)
);

CREATE TABLE Programma(
    IDPro INT NOT NULL AUTO_INCREMENT,
    DaIni DATETIME NULL,
    DaFin DATETIME NULL,
    Titolo VARCHAR(20) NULL,
    NomeSala VARCHAR(20) NULL,
    FOREIGN KEY(Titolo) REFERENCES Speech(Titolo),
    FOREIGN KEY(NomeSala) REFERENCES Sala(NomeSala),
    PRIMARY KEY(IDPro)
);

CREATE TABLE Relaziona(
    IDRel INT NOT NULL,
    IDPro INT NOT NULL,
    FOREIGN KEY(IDRel) REFERENCES Relatore(IDRel),
    FOREIGN KEY(IDPro) REFERENCES Programma(IDPro),
    PRIMARY KEY(IDRel, IDPro)
);

CREATE TABLE Sceglie(
    IDUtente INT NOT NULL,
    IDPro INT NOT NULL,
    FOREIGN KEY(IDUtente) REFERENCES Utente(IDUtente),
    FOREIGN KEY(IDPro) REFERENCES Programma(IDPro),
    PRIMARY KEY(IDPro, IDUtente)
);
