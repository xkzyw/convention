USE Convention;

-- Azienda
INSERT INTO Azienda (RagSocAz, IndAz, TelAz) VALUES
('Company1', 'Address1', '1234567890'),
('Company2', 'Address2', '0987654321'),
('Company3', 'Address3', '9876543210'),
('Company4', 'Address4', '4567890123'),
('Company5', 'Address5', '3210987654'),
('Company6', 'Address6', '2345678901'),
('Company7', 'Address7', '8901234567'),
('Company8', 'Address8', '6789012345'),
('Company9', 'Address9', '5432109876'),
('Company10', 'Address10', '2109876543');

-- Utente
INSERT INTO Utente (Psw, Username, Mail, Nome, Cognome) VALUES
('0b14d501a594442a01c6859541bcb3e8164d183d32937b851835442f69d5c94e', 'user1', 'user1@example.com', 'John', 'Doe'),
('8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 'admin', 'user2@example.com', 'Jane', 'Doe'),
('password3', 'user3', 'user3@example.com', 'Alice', 'Smith'),
('password4', 'user4', 'user4@example.com', 'Bob', 'Johnson'),
('password5', 'user5', 'user5@example.com', 'Emily', 'Brown'),
('password6', 'user6', 'user6@example.com', 'Michael', 'Davis'),
('password7', 'user7', 'user7@example.com', 'Emma', 'Wilson'),
('password8', 'user8', 'user8@example.com', 'William', 'Martinez'),
('password9', 'user9', 'user9@example.com', 'Olivia', 'Taylor'),
('password10', 'user10', 'user10@example.com', 'James', 'Anderson');

-- Relatore
INSERT INTO Relatore (IDRel, RagSocAz) VALUES
(1, 'Company1'),
(2, 'Company2'),
(3, 'Company3'),
(4, 'Company4'),
(5, 'Company5'),
(6, 'Company6'),
(7, 'Company7'),
(8, 'Company8'),
(9, 'Company9'),
(10, 'Company10');

-- Speech
INSERT INTO Speech (Titolo, Argomento) VALUES
('Speech1', 'Topic1'),
('Speech2', 'Topic2'),
('Speech3', 'Topic3'),
('Speech4', 'Topic4'),
('Speech5', 'Topic5'),
('Speech6', 'Topic6'),
('Speech7', 'Topic7'),
('Speech8', 'Topic8'),
('Speech9', 'Topic9'),
('Speech10', 'Topic10');

-- Piano
INSERT INTO Piano () VALUES
(DEFAULT), (DEFAULT), (DEFAULT), (DEFAULT), (DEFAULT),
(DEFAULT), (DEFAULT), (DEFAULT), (DEFAULT), (DEFAULT);

-- Sala
INSERT INTO Sala (NomeSala, NpostiSala, Numero) VALUES
('Room1', 100, 1),
('Room2', 80, 2),
('Room3', 120, 3),
('Room4', 150, 4),
('Room5', 200, 5),
('Room6', 90, 6),
('Room7', 110, 7),
('Room8', 70, 8),
('Room9', 180, 9),
('Room10', 160, 10);

-- Programma
INSERT INTO Programma (DaIni, DaFin, Titolo, NomeSala) VALUES
( '2024-04-15 09:00:00', '2024-04-15 10:30:00', "Speech1", 'Room1'),
( '2024-04-15 10:45:00', '2024-04-15 12:00:00', "Speech2", 'Room2'),
( '2024-04-15 13:00:00', '2024-04-15 14:30:00', "Speech3", 'Room3'),
( '2024-04-15 14:45:00', '2024-04-15 16:00:00', "Speech4", 'Room4'),
( '2024-04-15 16:15:00', '2024-04-15 17:30:00', "Speech5", 'Room5'),
( '2024-04-15 09:00:00', '2024-04-15 10:30:00', "Speech6", 'Room6'),
( '2024-04-15 10:45:00', '2024-04-15 12:00:00', "Speech7", 'Room7'),
( '2024-04-15 13:00:00', '2024-04-15 14:30:00', "Speech8", 'Room8'),
( '2024-04-15 14:45:00', '2024-04-15 16:00:00', "Speech9", 'Room9'),
( '2024-04-15 16:15:00', '2024-04-15 17:30:00', "Speech10", 'Room10');

