<pre>
<?php
    require "./config.php";
    require "./class/Connection.php";

    if(isset($_COOKIE["conv"])){
        echo "Utenti già autenticato</br>";
        echo "<a href='./index.php'>Home</a>";
        exit;
    }

    if(isset($_POST["signin"])){
        $username = $_POST["username"];
        $password = hash("sha256", $_POST["password"]);
        $email = $_POST["email"];
        $nome = $_POST["nome"];
        $cognome = $_POST["cognome"];

        if(empty($username) || empty($password || empty($email) || empty($nome) || empty($cognome))){
            echo "Dati non completi</br>";
            echo "<a href='./login.php'>Login</a>";
        }else{
            session_start();
            Connection::connect();

            $query = "INSERT INTO Utente VALUES (DEFAULT, ?, ?, ?, ?, ?)";
            $pq = Connection::$db->prepare($query);
            $pq->bind_param("sssss", $password, $username, $email, $nome, $cognome);
            $pq->execute();

            $query = "SELECT last_insert_id() as id;";
            $pq = Connection::$db->query($query);
            $lid = $pq->fetch_assoc()["id"];

            Connection::$db->close();

            $_SESSION["id_utente"] = $lid;
            $_SESSION["nome"] = $nome;
            $_SESSION["cognome"] = $cognome;
            $_SESSION["username"] = $username;

            $_SESSION["tipo"] = "utente";

            setcookie("conv", $lid, time() + $COOKIE_DURATION);

            echo "Registrato con successo <a href='./index.php'>Home</a>";
        }
    }
?>
</pre>