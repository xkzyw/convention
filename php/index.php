<?php
	require "./utils.php";
?>
<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Convention - Zhou</title>
    <link rel="stylesheet" href="../public/css/style.css">
  </head>
  <body>
		<div class="no-scroll">
			<div class="border-y">
				<div class="flex between mx-3 my-1">
					<div>
						Home
					</div>
					<div>
						<a href="./contents/speech.php">Speech</a>
					</div>
					<div>
						<a href="./contents/aziende.php">Aziende</a>
					</div>
					<div>
						<?php login_status()?>
					</div>
					<?php
						session_start();
						if(isset($_COOKIE["conv"]) && $_SESSION["tipo"] == "admin"){
					?>
						<div>
							<a href="./ar/area_riservata_adm.php">Area riservata</a>
						</div>
					<?php } else if(isset($_COOKIE["conv"]) && $_SESSION["tipo"] == "relatori") { ?>
						<div>
							<a href="./ar/area_riservata_rel.php">Area riservata</a>
						</div>
					<?php } else if(isset($_COOKIE["conv"]) && $_SESSION["tipo"] == "utente") { ?>
						<div>
							<a href="./ar/area_riservata_ute.php">Area riservata</a>
						</div>
					<?php } ?>
				</div>
			</div>
			<div class="h-100">
				<div class="title h-50 flex flex-center">
					<div>
						Convention
					</div>
				</div>
				<div class="h-50">
					<div class="h-50 flex h-center between mx-3">
						<div>
							<div class="h4">Quando:</div>
							<div class="h7">2024</div>
						</div>
						<div>
							<div class="h4">Dove:</div>
							<div class="h7">Verzuolo</div>
						</div>
						<div>
							<div class="h4"><a href="./contents/speech.php">Speech</a></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<script src="../public/js/script.js"></script>
  </body>
</html>
