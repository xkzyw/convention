<?php
    require "./config.php";
    require "./class/Connection.php";

    if(isset($_COOKIE["conv"])){
        echo "Utenti già autenticato</br>";
        echo "<a href='./index.php'>Home</a>";
        exit;
    }

    if(isset($_POST["login"])){
        session_start();

        $username = $_POST["username"];
        $password = hash("sha256", $_POST["password"]);

        if(empty($username) || empty($password)){
            echo "Username o password non inserito</br>";
            echo "<a href='./login.php'>Login</a>";
        }else{
            $query = "SELECT * FROM Utente WHERE Username = ? AND Psw = ?;";

            Connection::connect();
            
            $pq = Connection::$db->prepare($query);
            $pq->bind_param("ss", $username, $password);
            $pq->execute();
            $user = $pq->get_result()->fetch_assoc();

            if($user){
                $_SESSION["id_utente"] = $user["IDUtente"];
                $_SESSION["nome"] = $user["Nome"];
                $_SESSION["cognome"] = $user["Cognome"];
                $_SESSION["username"] = $user["Username"];

                if($user["Username"] == "admin"){
                    $_SESSION["tipo"] = "admin";
                }else{
                    $_SESSION["tipo"] = "utente";
                }

                setcookie("conv", $user["IDUtente"], time() + $COOKIE_DURATION);

                echo "Autenticato con successo</br>";
                echo "<a href='./index.php'>Home</a>";
            }else{
                echo "Utente non esistente o password sbagliato</br>";
                echo "<a href='./login.php'>Login</a>";
            }
        }
    }
?>