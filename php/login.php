<?php
    require "./config.php";

    if(isset($_COOKIE["conv"])){
        echo "Utenti già autenticato</br>";
        echo "<a href='./index.php'>Home</a>";
        exit;
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
</head>
<body>
    <form action="./login_page.php" method="POST">
        Username: <input type="text" name="username"/></br>
        Password: <input type="password" name="password"/></br>
        <input type="submit" value="Login" name="login"/></br>
        <a href="./signin.php">Sign in</a>
    </form>
</body>
</html>