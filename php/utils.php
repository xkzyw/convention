<?php
    require "./class/Connection.php";

    function is_valid_user($username) {
        Connection::connect();

        $q = "SELECT * FROM Utente WHERE Username = ?;";

        $pq = Connection::$db->prepare($q);

        $pq->bind_param("s", $username);
        $pq->execute();

        $res = $pq->get_result();

        Connection::$db->close();

        if($res->num_rows != 0){
            return true;
        }

        return false;
    }

    function login_status(){
        if(!$_COOKIE["conv"]){
            echo "<a href='./login.php'>Login</a>";
        }else{
            echo "<a href='./logout.php'>Logout</a>";
        }
    }
?>