<?php
    require "../config.php";
    require "../class/Connection.php";
    require "../utils.php";

    $query = "SELECT * FROM Speech;";

    Connection::connect();
    $res = Connection::$db->query($query);
    Connection::$db->close();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../../public/css/style.css">
</head>
<body>
    <div class="border-y">
      <div class="flex between mx-3 my-1">
        <div>
          <a href="../index.php">Home</a>
        </div>
        <div>
            Speech
        </div>
        <div>
            <a href="./aziende.php">Aziende</a>
        </div>
        <div>
          <?php login_status()?>
        </div>
        <?php
						session_start();
						if(isset($_COOKIE["conv"]) && $_SESSION["tipo"] == "admin"){
					?>
						<div>
							<a href="../ar/area_riservata_adm.php">Area riservata</a>
						</div>
					<?php } else if(isset($_COOKIE["conv"]) && $_SESSION["tipo"] == "relatori") { ?>
						<div>
							<a href="../ar/area_riservata_rel.php">Area riservata</a>
						</div>
					<?php } else if(isset($_COOKIE["conv"]) && $_SESSION["tipo"] == "utente") { ?>
						<div>
							<a href="../ar/area_riservata_ute.php">Area riservata</a>
						</div>
					<?php } ?>
      </div>
    </div>

    <div class="flex flex-center my-3">
      <div class="title">      
        Speech
      </div>
    </div>
    <?php
        if($res->num_rows == 0){
    ?>
        <div class="flex flex-center">
                <div class="flex between">
                  <div class="flex flex-center">
                      <div class="h4">
                        Non Speech disponibili al momento.
                      </div>
                  </div>
              </div>
            </div>
    <?php  exit; }?>
    <?php
        foreach($res as $row){
    ?>
            <div class="flex flex-center border-top">
              <div class="wv-40">
                <div class="flex between">
                  <div class="flex flex-center">
                      <div class="h4">
                        <?=$row["Titolo"]?>
                      </div>
                  </div>
                  <div class="my-3">
                    <div>
                        <?=$row["Argomento"]?>
                    </div>
                  </div>
                  <div class="my-3">
                    <div>
                        <form action="./programmi.php" method="GET">
                            <input type="submit" value="Dettagli">
                            <input type="hidden" name="spec" value=<?=$row["Titolo"]?>>
                        </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
    <?php } ?>
</body>
</html>