<?php
    require "../config.php";
    require "../class/Connection.php";
    require "../utils.php";

    if(!isset($_GET["spec"])){
        echo "Non speech scelto. <a href='./speech.php'>Scegli un speech</a>";
        exit;
    }
    session_start();

    $query = "SELECT * FROM Programma WHERE Titolo = ? ORDER BY DaIni;";

    $titolo = $_GET["spec"];
    Connection::connect();
    $pq = Connection::$db->prepare($query);
    $pq->bind_param("s", $titolo);
    $pq->execute();
    $res = $pq->get_result()->fetch_all(MYSQLI_ASSOC);

    Connection::$db->close();
?>

<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Zhou - 5L</title>
    <link rel="stylesheet" href="../../public/css/style.css">
</head>
<body>
    <div class="border-y">
      <div class="flex between mx-3 my-1">
        <div>
          <a href="../index.php">Home</a>
        </div>
        <div>
            <a href="../speech.php">Speech</a>
        </div>
        <div>
            <a href="./aziende.php">Aziende</a>
        </div>
        <div>
          <?php login_status()?>
        </div>
        <?php
						if(isset($_COOKIE["conv"]) && $_SESSION["tipo"] == "admin"){
					?>
						<div>
							<a href="./ar/area_riservata_adm.php">Area riservata</a>
						</div>
					<?php } else if(isset($_COOKIE["conv"]) && $_SESSION["tipo"] == "relatori") { ?>
						<div>
							<a href="./ar/area_riservata_rel.php">Area riservata</a>
						</div>
					<?php } else if(isset($_COOKIE["conv"]) && $_SESSION["tipo"] == "utente") { ?>
						<div>
							<a href="./ar/area_riservata_ute.php">Area riservata</a>
						</div>
					<?php } ?>
      </div>
    </div>

    <div class="flex flex-center my-3">
      <div class="title">      
        Programmi del <?=$titolo?>
      </div>
    </div>
    <?php
        if(count($res) == 0){

    ?>
        <div class="flex flex-center">
                <div class="flex between">
                  <div class="flex flex-center">
                      <div class="h4">
                        Non programmi disponibili al momento.
                      </div>
                  </div>
              </div>
            </div>
    <?php exit; } ?>

    <?php
        $i = 0;
        foreach($res as $row){
            Connection::connect();
            $query = "SELECT COUNT(*) as num FROM Sceglie WHERE IDPro = ?;";

            $pq = Connection::$db->prepare($query);
            $pq->bind_param("i", $row["IDPro"]);
            $pq->execute();
            $num_occupato = $pq->get_result()->fetch_assoc()["num"];

            $query = "SELECT * FROM Sala WHERE NomeSala = ?;";

            $pq = Connection::$db->prepare($query);
            $pq->bind_param("s", $row["NomeSala"]);
            $pq->execute();
            $sala = $pq->get_result()->fetch_assoc();

            Connection::$db->close();
    ?>
            <div class="flex flex-center border-top">
              <div class="wv-40">
                <div class="flex between">
                  <div class="flex flex-center">
                      <div class="h4">
                        Programma <?=++$i?>
                      </div>
                  </div>
                  <div class="my-3">
                    <div>
                        <?=$row["DaIni"]?>
                    </div>
                  </div>
                  <div class="my-3">
                    <div>
                        <?=$row["DaFin"]?>
                    </div>
                  </div>
                  <div class="my-3">
                    <div>
                        Sala: <?=$row["NomeSala"]?>
                    </div>
                  </div>
                  <div class="my-3">
                    <div>
                        Posti disponibili: <?=$sala["NpostiSala"] - $num_occupato?>
                    </div>
                  </div>
                  <?php
                    if($num_occupato < $sala["NpostiSala"] && isset($_COOKIE["conv"])){
                        Connection::connect();
                        $query = "SELECT COUNT(*) as num FROM Sceglie WHERE IDPro = ? AND IDUtente = ?;";

                        $pq = Connection::$db->prepare($query);
                        $pq->bind_param("ii", $row["IDPro"], $_COOKIE["conv"]);
                        $pq->execute();
                        $num = $pq->get_result()->fetch_assoc()["num"];
                        Connection::$db->close();

                        if($num == 0){
                    ?>
                  <div class="my-3">
                    <div>
                        <form action="../prenota_posti.php" method="post">
                            <input type="submit" value="Prenota">
                            <input type="hidden" name="programma" value=<?=$row["IDPro"]?>>
                        </form>
                    </div>
                  </div>
                  <?php } else {?>
                    <div class="my-3">
                        <div>
                            Prenotato
                        </div>
                  </div>
                <?php } }?>
                </div>
              </div>
            </div>
    <?php } ?>
</body>
</html>