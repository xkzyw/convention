<?php
    require "../config.php";
    require "../class/Connection.php";
    require "../utils.php";

    $query = "SELECT * FROM Azienda;";

    Connection::connect();
    $res = Connection::$db->query($query);
    Connection::$db->close();

    function fromSQL($res): array {
        $aziende = [];

        foreach($res as $row){
            $aziende[] = (object) $row;
        }

        return $aziende;
    }

    $aziende = fromSQL($res);
?>

<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Convention - Zhou</title>
    <link rel="stylesheet" href="../../public/css/style.css">
  </head>
  <body>
  <div class="border-y">
      <div class="flex between mx-3 my-1">
        <div>
          <a href="../index.php">Home</a>
        </div>
        <div>
          <a href="./speech.php">Speech</a>
        </div>
        <div>
          Aziende
        </div>
        <div>
          <?php login_status()?>
        </div>
        <?php
						session_start();
						if(isset($_COOKIE["conv"]) && $_SESSION["tipo"] == "admin"){
					?>
						<div>
							<a href="../ar/area_riservata_adm.php">Area riservata</a>
						</div>
					<?php } else if(isset($_COOKIE["conv"]) && $_SESSION["tipo"] == "relatori") { ?>
						<div>
							<a href="../ar/area_riservata_rel.php">Area riservata</a>
						</div>
					<?php } else if(isset($_COOKIE["conv"]) && $_SESSION["tipo"] == "utente") { ?>
						<div>
							<a href="../ar/area_riservata_ute.php">Area riservata</a>
						</div>
					<?php } ?>
      </div>
    </div>

    <div class="flex flex-center my-3">
      <div class="title">      
        Aziende partecipante
      </div>
    </div>

    <?php
        if(count($aziende) == 0){
    ?>
        <div class="flex flex-center">
                <div class="flex between">
                  <div class="flex flex-center">
                      <div class="h4">
                        Non Azeidne disponibili al momento.
                      </div>
                  </div>
              </div>
            </div>
    <?php exit; } ?>

    <div>
        <?php
          foreach($aziende as $azienda){
        ?>
            <div class="flex flex-center border-top">
              <div class="wv-40">
                <div class="flex between">
                  <div class="flex flex-center">
                      <div class="h4">
                        <?=$azienda->RagSocAz?>
                      </div>
                  </div>
                  <div class="flex flex-center my-3">
                    <div>
                        <?="Tel: ".$azienda->TelAz?>
                    </div>
                  </div>
                  <div class="flex flex-center">
                    <div>
                      <?="Indirizzo: ".$azienda->IndAz?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        <?php } ?>
    </div>
  </body>
</html>