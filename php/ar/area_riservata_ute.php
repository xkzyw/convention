<?php
    require "../config.php";
    require "../utils.php";
    require "../class/Connection.php";

    if(!isset($_COOKIE["conv"])){
        echo "Utenti non autenticato.</br>";
        echo "<a href='../index.php'>Home</a>";
        exit;
    }

    session_start();

    $uid = $_SESSION["id_utente"];

    $query = "SELECT * FROM Relatore WHERE IDRel = ?;";

    Connection::connect();
    $pq = Connection::$db->prepare($query);
    $pq->bind_param("i", $uid);
    $pq->execute();
    $res = $pq->get_result();

    $query = "SELECT * FROM Utente WHERE IDUtente = ?;";
    $pq = Connection::$db->prepare($query);
    $pq->bind_param("i", $uid);
    $pq->execute();
    $utente = $pq->get_result()->fetch_assoc();


    if($res->num_rows == 0){
        $is_relatore = false;
    }else{
        $is_relatore = true;
    }

    if($is_relatore){
        $query = "SELECT * FROM Sala JOIN (Programma JOIN (Relaziona JOIN Relatore ON Relaziona.IDRel = Relatore.IDRel) ON Relaziona.IDRel = Programma.IDPro) ON Sala.NomeSala = Programma.NomeSala WHERE Relatore.IDRel = ?;";
        $pq = Connection::$db->prepare($query);
        $pq->bind_param("i", $uid);
        $pq->execute();
        $programmi = $pq->get_result();
    }

    $query = "SELECT * FROM Sala JOIN (Programma JOIN (Sceglie JOIN Utente ON Sceglie.IDUtente = Utente.IDUtente) ON Sceglie.IDUtente = Programma.IDPro) ON Sala.NomeSala = Programma.NomeSala WHERE Utente.IDUtente = ?;";
    $pq = Connection::$db->prepare($query);
    $pq->bind_param("i", $uid);
    $pq->execute();
    $programmi_partecipate = $pq->get_result();

    Connection::$db->close();

?>
<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>5L - Zhou</title>
    <link rel="stylesheet" href="../../public/css/style.css">
</head>
<body>
    <div class="border-y">
      <div class="flex between mx-3 my-1">
        <div>
          <a href="../index.php">Home</a>
        </div>
        <div>
            <a href="../contents/speech.php">Speech</a>
        </div>
        <div>
            <a href="../contents/aziende.php">Aziende</a>
        </div>
        <div>
          <?php login_status()?>
        </div>
        <?php
            if(isset($_COOKIE["conv"]) && $_SESSION["tipo"] == "admin"){
        ?>
            <div>
                <a href="./area_riservata_adm.php">Area riservata</a>
            </div>
        <?php } else if(isset($_COOKIE["conv"]) && $_SESSION["tipo"] == "relatori") { ?>
            <div>
                <a href="./area_riservata_rel.php">Area riservata</a>
            </div>
        <?php } else if(isset($_COOKIE["conv"]) && $_SESSION["tipo"] == "utente") { ?>
            <div>
                <a href="./area_riservata_ute.php">Area riservata</a>
            </div>
        <?php } ?>
      </div>
    </div>

    <div class="flex flex-center my-3">
      <div class="title">      
        Benvenuto <?=$utente["Nome"]?>
      </div>
    </div>

    <?php 
        if($is_relatore){
    ?>
        <div class="flex flex-center my-3">
            <div class="h4">      
                Programmi partecipate come relatore
            </div>
        </div>

        <table>
            <tr>
                <th>Nome speech</th>
                <th>Sala</th>
                <th>Piano</th>
                <th>Azienda</th>
                <th>Data inizio</th>
                <th>Data fine</th>
            </tr>
            <?php foreach($programmi as $p){ ?>
                <tr>
                    <td><?=$p["Titolo"]?></td>
                    <td><?=$p["NomeSala"]?></td>
                    <td><?=$p["Numero"]?></td>
                    <td><?=$p["RagSocAz"]?></td>
                    <td><?=$p["DaIni"]?></td>
                    <td><?=$p["DaFin"]?></td>
                </tr>
            <?php }?>
        </table>
        <?php } ?>

        <div class="flex flex-center my-3">
            <div class="h4">      
                Programmi partecipate
            </div>
        </div>
        
    <?php
        if($programmi_partecipate->num_rows != 0){
    ?>
    <table>
        <tr>
            <th>Nome speech</th>
            <th>Sala</th>
            <th>Piano</th>
            <th>Data inizio</th>
            <th>Data fine</th>
        </tr>
        <?php foreach($programmi_partecipate as $p){ ?>
            <tr>
                <td><?=$p["Titolo"]?></td>
                <td><?=$p["NomeSala"]?></td>
                <td><?=$p["Numero"]?></td>
                <td><?=$p["DaIni"]?></td>
                <td><?=$p["DaFin"]?></td>
            </tr>
        <?php }?>
    </table>
    <?php } else {?>
        Non programma partecipato
    <?php } ?>
</body>
</html>