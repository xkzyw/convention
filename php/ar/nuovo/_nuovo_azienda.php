<?php
    require "../../config.php";
    require "../../class/Connection.php";

    if(!isset($_COOKIE["conv"])){
        echo "Utenti non autenticato.</br>";
        echo "<a href='../../index.php'>Home</a>";
        exit;
    }

    session_start();

    if($_SESSION["tipo"] != "admin"){
        echo "Utenti non permesso a visualizzare questa pagina</br>";
        echo "<a href='../../index.php'>Home</a>";
        exit;
    }

    if(! (isset($_POST["ragsoc"]) && isset($_POST["ind"]) && isset($_POST["tel"]) && !empty($_POST["ragsoc"] && !empty($_POST["ind"])  && !empty($_POST["tel"])))){
        echo "Dati non completi</br>";
        echo "<a href='./nuovo_azienda.php'>Ritorna in dietro</a>";
        exit;
    }

    $ragsoc = $_POST["ragsoc"];
    $ind = $_POST["ind"];
    $tel = $_POST["tel"];

    Connection::connect();

    $query = "SELECT COUNT(*) as num FROM Azienda WHERE RagSocAz = ?";
    $pq = Connection::$db->prepare($query);
    $pq->bind_param("s", $ragsoc);
    $pq->execute();

    $num_azienda = $pq->get_result()->fetch_assoc()["num"];

    if($num_azienda == 0){
        $query = "INSERT INTO Azienda VALUES (?, ?, ?);";
        $pq = Connection::$db->prepare($query);
        $pq->bind_param("sss", $ragsoc, $ind, $tel);
        $pq->execute();

        echo "Azinda aggiunto con successo</br>";
        echo "<a href='../area_riservata_adm.php'>Ritorna nell'area riservata</a>";
    }else{
        echo "Azienda già presente</br>";
        echo "<a href='../area_riservata_adm.php'>Ritorna nell'area riservata</a>";
    }
        
    Connection::$db->close();
?>