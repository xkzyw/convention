<?php
    require "../../config.php";
    require "../../class/Connection.php";
    require "../../utils.php";

    if(!isset($_COOKIE["conv"])){
        echo "Utenti non autenticato.</br>";
        echo "<a href='../../index.php'>Home</a>";
        exit;
    }

    session_start();

    if($_SESSION["tipo"] != "admin"){
        echo "Utenti non permesso a visualizzare questa pagina</br>";
        echo "<a href='../../index.php'>Home</a>";
        exit;
    }

    if(! (isset($_POST["username"]) && isset($_POST["speech"]) && isset($_POST["ragsoc"]))){
        echo "Dati non completi</br>";
        echo "<a href='../../index.php'>Home</a>";
        exit;
    }

    $speech = $_POST["speech"];
    $ragsoc = $_POST["ragsoc"];
    $username = $_POST["username"];
    $sala = $_POST["sala"];

    Connection::connect();

    $query = "SELECT * FROM Programma WHERE Titolo = ? AND NomeSala = ?;";
    $pq = Connection::$db->prepare($query);
    $pq->bind_param("ss", $speech, $sala);
    $pq->execute();
    $programma = $pq->get_result();
        
    Connection::$db->close();

?>
<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>5L - Zhou</title>
    <link rel="stylesheet" href="../../../public/css/style.css">
</head>
<body>
<div class="border-y">
      <div class="flex between mx-3 my-1">
        <div>
          <a href="../../index.php">Home</a>
        </div>
        <div>
            <a href="../../contents/speech.php">Speech</a>
        </div>
        <div>
            <a href="../../contents/aziende.php">Aziende</a>
        </div>
        <div>
            <?php login_status()?>
        </div>
        <?php
            if(isset($_COOKIE["conv"]) && $_SESSION["tipo"] == "admin"){
        ?>
            <div>
                <a href="../area_riservata_adm.php">Area riservata</a>
            </div>
        <?php } else if(isset($_COOKIE["conv"]) && $_SESSION["tipo"] == "relatori") { ?>
            <div>
                <a href="../area_riservata_rel.php">Area riservata</a>
            </div>
        <?php } else if(isset($_COOKIE["conv"]) && $_SESSION["tipo"] == "utente") { ?>
            <div>
                <a href="../area_riservata_ute.php">Area riservata</a>
            </div>
        <?php } ?>
      </div>
    </div>

    <div class="flex flex-center my-3">
      <div class="title">      
        Nuovo programma
      </div>
    </div>

    <div>
        <form action="./_nuovo_pro.php" method="POST">
            <input type="hidden" name="sala" value=<?=$sala?>>
            <input type="hidden" name="ragsoc" value=<?=$ragsoc?>>
            <input type="hidden" name="username" value=<?=$username?>>
            <input type="hidden" name="speech" value=<?=$speech?>>

            Scegli la fascia oraria: </br>
            <?php
                $i = 0;
                foreach($ORARIO as $key => $or){
                    foreach($programma as $pro){
                        $dtIni = new DateTime($DATA_EVENTO." ".$or["start"]);
                        $dtFin = new DateTime($DATA_EVENTO." ".$or["end"]);

                        $proIni = new DateTime($pro["DaIni"]);
                        $proFin = new DateTime($pro["DaFin"]);

                        if($proIni == $dtIni && $proFin == $dtFin){
                            $i++;
                            continue 2;
                        }
                    }

            ?>
                <input required type="radio" name="orario" value=<?=$key?>><?=$or["start"]." - ".$or["end"]?></br>
            <?php } 
                if($i == 4){
            ?>
                <div>Non posti disponibile</div>
            <?php }else{?>
                <input type="submit" value="Aggiungi">
            <?php }?>
        </form>
    </div>
</body>
</html>