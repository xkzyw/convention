<?php
    require "../../config.php";
    require "../../class/Connection.php";
    require "../../utils.php";

    if(!isset($_COOKIE["conv"])){
        echo "Utenti non autenticato.</br>";
        echo "<a href='../../index.php'>Home</a>";
        exit;
    }

    session_start();

    if($_SESSION["tipo"] != "admin"){
        echo "Utenti non permesso a visualizzare questa pagina</br>";
        echo "<a href='../../index.php'>Home</a>";
        exit;
    }

    Connection::connect();

    $query = "SELECT Username FROM Utente LEFT JOIN Relatore ON Utente.IDUtente = Relatore.IDRel WHERE Utente.Username != 'admin'";
    $result = Connection::$db->query($query);

    $query = "SELECT RagSocAz FROM Azienda;";
    $aziende = Connection::$db->query($query);

    $query = "SELECT * FROM Speech;";
    $speech = Connection::$db->query($query);

    $query = "SELECT * FROM Sala;";
    $sala = Connection::$db->query($query);

    Connection::$db->close();

?>
<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>5L - Zhou</title>
    <link rel="stylesheet" href="../../../public/css/style.css">
</head>
<body>
    <div class="border-y">
      <div class="flex between mx-3 my-1">
        <div>
          <a href="../../index.php">Home</a>
        </div>
        <div>
            <a href="../../contents/speech.php">Speech</a>
        </div>
        <div>
            <a href="../../contents/aziende.php">Aziende</a>
        </div>
        <div>
            <?php login_status()?>
        </div>
        <?php
            if(isset($_COOKIE["conv"]) && $_SESSION["tipo"] == "admin"){
        ?>
            <div>
                <a href="../area_riservata_adm.php">Area riservata</a>
            </div>
        <?php } else if(isset($_COOKIE["conv"]) && $_SESSION["tipo"] == "relatori") { ?>
            <div>
                <a href="../area_riservata_rel.php">Area riservata</a>
            </div>
        <?php } else if(isset($_COOKIE["conv"]) && $_SESSION["tipo"] == "utente") { ?>
            <div>
                <a href="../area_riservata_ute.php">Area riservata</a>
            </div>
        <?php } ?>
      </div>
    </div>

    <div class="flex flex-center my-3">
      <div class="title">      
        Nuovo relatore
      </div>
    </div>

    <div>
        <?php
            if($result->num_rows != 0){
        ?>
            <form action="./nuovo_pro.php" method="POST">
                <div>
                    Utente: 
                    <select name="username">
                        <?php
                            foreach($result as $username){
                        ?>
                            <option><?=$username["Username"]?></option>
                        <?php } ?>
                    </select>
                </div>
                <div>
                    Azienda: 
                    <select name="ragsoc">
                        <?php
                            foreach($aziende as $azienda){
                        ?>
                            <option><?=$azienda["RagSocAz"]?></option>
                        <?php } ?>
                    </select>
                </div>
                <div>
                    Speech: 
                    <select name="speech">
                        <?php
                            foreach($speech as $sp){
                        ?>
                            <option><?=$sp["Titolo"]?></option>
                        <?php } ?>
                    </select>
                </div>
                <div>
                    Sala: 
                    <select name="sala">
                        <?php
                            foreach($sala as $sp){
                        ?>
                            <option><?=$sp["NomeSala"]?></option>
                        <?php } ?>
                    </select>
                </div>
                <div>
                    Data: 
                    <?=$DATA_EVENTO?>
                </div>
                <input type="submit" value="Sceglie il programma">
            </form>
        <?php }else{ ?>
            <div>Non utente presente o che tutti sono relatori.</div>
        <?php }?>
    </div>
</body>
</html>