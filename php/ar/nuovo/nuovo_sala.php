<?php
    require "../../config.php";
    require "../../class/Connection.php";
    require "../../utils.php";

    if(!isset($_COOKIE["conv"])){
        echo "Utenti non autenticato.</br>";
        echo "<a href='../../index.php'>Home</a>";
        exit;
    }

    session_start();

    if($_SESSION["tipo"] != "admin"){
        echo "Utenti non permesso a visualizzare questa pagina</br>";
        echo "<a href='../../index.php'>Home</a>";
        exit;
    }

    Connection::connect();

    $query = "SELECT * FROM Piano;";
    $piano = Connection::$db->query($query);

    Connection::$db->close();
?>
<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>5L - Zhou</title>
    <link rel="stylesheet" href="../../../public/css/style.css">
</head>
<body>
<div class="border-y">
      <div class="flex between mx-3 my-1">
        <div>
          <a href="../../index.php">Home</a>
        </div>
        <div>
            <a href="../../contents/speech.php">Speech</a>
        </div>
        <div>
            <a href="../../contents/aziende.php">Aziende</a>
        </div>
        <div>
            <?php login_status()?>
        </div>
        <?php
            if(isset($_COOKIE["conv"]) && $_SESSION["tipo"] == "admin"){
        ?>
            <div>
                <a href="../area_riservata_adm.php">Area riservata</a>
            </div>
        <?php } else if(isset($_COOKIE["conv"]) && $_SESSION["tipo"] == "relatori") { ?>
            <div>
                <a href="../area_riservata_rel.php">Area riservata</a>
            </div>
        <?php } else if(isset($_COOKIE["conv"]) && $_SESSION["tipo"] == "utente") { ?>
            <div>
                <a href="../area_riservata_ute.php">Area riservata</a>
            </div>
        <?php } ?>
      </div>
    </div>

    <div class="flex flex-center my-3">
      <div class="title">      
        Nuovo Sala
      </div>
    </div>

    <div>
       <form action="./_nuovo_sala.php" method="post">
            Nome sala: 
            <input type="text" name="nome"><br>
            Numero di posti: 
            <input type="number" name="nposti" min="1"><br>
            Piano: 
            <select name="piano">
                <?php
                    foreach($piano as $p){
                ?>
                    <option><?=$p["Numero"]?></option>
                <?php } ?>
            </select><br>
            <input type="submit" value="Aggiungi">
       </form>
    </div>
</body>
</html>