<?php
    require "../../config.php";
    require "../../class/Connection.php";

    if(!isset($_COOKIE["conv"])){
        echo "Utenti non autenticato.</br>";
        echo "<a href='../../index.php'>Home</a>";
        exit;
    }

    session_start();

    if($_SESSION["tipo"] != "admin"){
        echo "Utenti non permesso a visualizzare questa pagina</br>";
        echo "<a href='../../index.php'>Home</a>";
        exit;
    }

    if(! (isset($_POST["titolo"]) && isset($_POST["argomento"]) && !empty($_POST["titolo"] && !empty($_POST["argomento"])))){
        echo "Dati non completi</br>";
        echo "<a href='./nuovo_speech.php'>Ritorna in dietro</a>";
        exit;
    }

    $titolo = $_POST["titolo"];
    $argomento = $_POST["argomento"];

    Connection::connect();

    $query = "SELECT COUNT(*) as num FROM Speech WHERE Titolo = ?";
    $pq = Connection::$db->prepare($query);
    $pq->bind_param("s", $titolo);
    $pq->execute();

    $num_speech = $pq->get_result()->fetch_assoc()["num"];

    if($num_speech == 0){
        $query = "INSERT INTO Speech VALUES (?, ?);";
        $pq = Connection::$db->prepare($query);
        $pq->bind_param("ss", $titolo, $argomento);
        $pq->execute();

        echo "Speech aggiunto con successo</br>";
        echo "<a href='../area_riservata_adm.php'>Ritorna nell'area riservata</a>";
    }else{
        echo "Speech già presente</br>";
        echo "<a href='../area_riservata_adm.php'>Ritorna nell'area riservata</a>";
    }
        
    Connection::$db->close();
?>