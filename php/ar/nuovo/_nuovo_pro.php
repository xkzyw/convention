<?php
    require "../../config.php";
    require "../../class/Connection.php";

    if(!isset($_COOKIE["conv"])){
        echo "Utenti non autenticato.</br>";
        echo "<a href='../../index.php'>Home</a>";
        exit;
    }

    session_start();

    if($_SESSION["tipo"] != "admin"){
        echo "Utenti non permesso a visualizzare questa pagina</br>";
        echo "<a href='../../index.php'>Home</a>";
        exit;
    }

    if(! (isset($_POST["username"]) && isset($_POST["speech"]) && isset($_POST["ragsoc"]))){
        echo "Dati non completi</br>";
        echo "<a href='./nuovo_pro.php'>Ritorna in dietro</a>";
        exit;
    }

    $speech = $_POST["speech"];
    $ragsoc = $_POST["ragsoc"];
    $username = $_POST["username"];
    $sala = $_POST["sala"];
    $orario = $_POST["orario"];

    $dini = $DATA_EVENTO." ".$ORARIO[$orario]["start"];
    $dfin = $DATA_EVENTO." ".$ORARIO[$orario]["end"];

    Connection::connect();

    $query = "SELECT IDPro FROM Programma WHERE DaIni = ? AND DaFin = ? AND Titolo = ? AND NomeSala = ?";
    $pq = Connection::$db->prepare($query);
    $pq->bind_param("ssss", $dini, $dfin, $speech, $sala);
    $pq->execute();

    $proId = $pq->get_result()->fetch_assoc();

    if(!$proId){
        $query = "INSERT INTO Programma VALUES (DEFAULT, ?, ?, ?, ?)";
        $pq = Connection::$db->prepare($query);
        $pq->bind_param("ssss", $dini, $dfin, $speech, $sala);
        $pq->execute();
        $proId = $pq->insert_id;
    }else{
        $proId = $proId["IDPro"];
    }

    $query = "SELECT IDUtente as uid FROM Utente WHERE Username = ?;";
    $pq = Connection::$db->prepare($query);
    $pq->bind_param("s", $username);
    $pq->execute();

    $uid = $pq->get_result()->fetch_assoc()["uid"];

    $query = "SELECT COUNT(*) as num FROM Relatore WHERE IDRel = ?";
    $pq = Connection::$db->prepare($query);
    $pq->bind_param("i", $uid);
    $pq->execute();

    $is_rel = $pq->get_result()->fetch_assoc()["num"];
    $relId = $uid;

    if($is_rel == 0){
        $query = "INSERT INTO Relatore VALUES (?, ?)";
        $pq = Connection::$db->prepare($query);
        $pq->bind_param("is", $uid, $ragsoc);
        $pq->execute();
    }

    $query = "SELECT COUNT(*) as num FROM Relaziona WHERE IDRel = ? AND IDPro = ?";
    $pq = Connection::$db->prepare($query);
    $pq->bind_param("ii", $uid, $proId);
    $pq->execute();

    $is_scritto = $pq->get_result()->fetch_assoc()["num"];

    if($is_scritto == 0){
        $query = "INSERT INTO Relaziona VALUES (?, ?)";
        $pq = Connection::$db->prepare($query);
        $pq->bind_param("ii", $relId, $proId);
        $pq->execute();

        echo "Relatore aggiunto con successo</br>";
        echo "<a href='../area_riservata_adm.php'>Ritorna nell'area riservata</a>";
    }else{
        echo "Relatore già registrato per questa programma</br>";
        echo "<a href='../area_riservata_adm.php'>Ritorna nell'area riservata</a>";
    }
        
    Connection::$db->close();

    
?>