<?php
    require "../../config.php";
    require "../../class/Connection.php";

    if(!isset($_COOKIE["conv"])){
        echo "Utenti non autenticato.</br>";
        echo "<a href='../../index.php'>Home</a>";
        exit;
    }

    session_start();

    if($_SESSION["tipo"] != "admin"){
        echo "Utenti non permesso a visualizzare questa pagina</br>";
        echo "<a href='../../index.php'>Home</a>";
        exit;
    }

    if(! (isset($_POST["nome"]) && isset($_POST["nposti"]) && isset($_POST["piano"]) && !empty($_POST["piano"] && !empty($_POST["nome"])  && !empty($_POST["nposti"])))){
        echo "Dati non completi</br>";
        echo "<a href='./nuovo_sala.php'>Ritorna in dietro</a>";
        exit;
    }

    $nome = $_POST["nome"];
    $nposti = $_POST["nposti"];
    $piano = $_POST["piano"];

    Connection::connect();

    $query = "SELECT COUNT(*) as num FROM Sala WHERE NomeSala = ?";
    $pq = Connection::$db->prepare($query);
    $pq->bind_param("s", $nome);
    $pq->execute();

    $num_sala = $pq->get_result()->fetch_assoc()["num"];

    if($num_sala == 0){
        $query = "INSERT INTO Sala VALUES (?, ?, ?);";
        $pq = Connection::$db->prepare($query);
        $pq->bind_param("sss", $nome, $nposti, $piano);
        $pq->execute();

        echo "Sala aggiunto con successo</br>";
        echo "<a href='../area_riservata_adm.php'>Ritorna nell'area riservata</a>";
    }else{
        echo "Sala già presente</br>";
        echo "<a href='../area_riservata_adm.php'>Ritorna nell'area riservata</a>";
    }
        
    Connection::$db->close();
?>