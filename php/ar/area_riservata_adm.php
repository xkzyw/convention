<?php
    require "../config.php";
    require "../class/Connection.php";
    require "../utils.php";

    if(!isset($_COOKIE["conv"])){
        echo "Utenti non autenticato.</br>";
        echo "<a href='../index.php'>Home</a>";
        exit;
    }

    session_start();

    if($_SESSION["tipo"] != "admin"){
        echo "Utenti non permesso a visualizzare questa pagina</br>";
        echo "<a href='../index.php'>Home</a>";
        exit;
    }

?>
<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>5L - Zhou</title>
    <link rel="stylesheet" href="../../public/css/style.css">
</head>
<body>
    <div class="border-y">
      <div class="flex between mx-3 my-1">
        <div>
          <a href="../index.php">Home</a>
        </div>
        <div>
            <a href="../contents/speech.php">Speech</a>
        </div>
        <div>
            <a href="../contents/aziende.php">Aziende</a>
        </div>
        <div>
            <?php login_status()?>
        </div>
        <?php
            if(isset($_COOKIE["conv"]) && $_SESSION["tipo"] == "admin"){
        ?>
            <div>
                <a href="./area_riservata_adm.php">Area riservata</a>
            </div>
        <?php } else if(isset($_COOKIE["conv"]) && $_SESSION["tipo"] == "relatori") { ?>
            <div>
                <a href="./area_riservata_rel.php">Area riservata</a>
            </div>
        <?php } else if(isset($_COOKIE["conv"]) && $_SESSION["tipo"] == "utente") { ?>
            <div>
                <a href="./area_riservata_ute.php">Area riservata</a>
            </div>
        <?php } ?>
      </div>
    </div>

    <div class="flex flex-center my-3">
      <div class="title">      
        Benvenuto Amministratore
      </div>
    </div>

    <div>
        <a href="./nuovo/nuovo_rel.php">Nuovo relatore</a>
    </div>
    <div>
        <a href="./nuovo/nuovo_speech.php">Nuovo speech</a>
    </div>
    <div>
        <a href="./nuovo/nuovo_azienda.php">Nuovo Azienda</a>
    </div>
    <div>
        <!-- Contiene la nuova piano -->
        <a href="./nuovo/nuovo_sala.php">Nuovo Sala</a>
    </div>

    <div>
        <a href="./nuovo/nuovo_rel.php">Elimina relatore</a>
    </div>
    <div>
        <a href="./nuovo/nuovo_rel.php">Elimina speech</a>
    </div>
    <div>
        <a href="./nuovo/nuovo_rel.php">Elimina programma</a>
    </div>
    <div>
        <a href="./nuovo/nuovo_rel.php">Elimina Azienda</a>
    </div>
    <div>
        <!-- Contiene la nuova piano -->
        <a href="./nuovo/nuovo_rel.php">Elimina Sala</a>
    </div>
</body>
</html>