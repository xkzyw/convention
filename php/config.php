<?php
    ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);

    // ini_set('session.gc_maxlifetime', 360);

    // session_set_cookie_params(360);

    $COOKIE_DURATION = 3600;

    $DATA_EVENTO = "2024-04-15";
    $ORARIO = [
        [
            "start" => "8:00:00",
            "end" => "10:00:00"
        ],
        [
            "start" => "10:00:00",
            "end" => "12:00:00"
        ],
        [
            "start" => "14:00:00",
            "end" => "16:00:00"
        ],
        [
            "start" => "16:00:00",
            "end" => "18:00:00"
        ]
    ]
?>