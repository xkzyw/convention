<?php
    class Connection
    {
        public static string $ip = "localhost";
        public static string $user = "root";
        public static string $password = "";
        public static string $db_name = "Convention";

        public static mysqli $db;

        public static function connect()
        {
            self::$db = new mysqli(self::$ip, self::$user, self::$password, self::$db_name);
        }

    }
?>