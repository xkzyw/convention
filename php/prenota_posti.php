<?php
    require "./config.php";
    require "./class/Connection.php";

    if(!isset($_COOKIE["conv"])){
        echo "Utenti non autenticato. Fai il ";
        echo "<a href='./login.php'>Login</a>";
        exit;
    }

    if(!isset($_POST["programma"])){
        echo "Non programma scelto.";
        echo "<a href='./contents/speech.php'>Speech</a>";
        exit;
    }

    $id_utente = $_COOKIE["conv"];
    $id_pro = $_POST["programma"];

    Connection::connect();
    $query = "SELECT COUNT(*) as num FROM Sceglie WHERE IDPro = ? AND IDUtente = ?;";

    $pq = Connection::$db->prepare($query);
    $pq->bind_param("ii", $_POST["programma"], $_COOKIE["conv"]);
    $pq->execute();
    $num = $pq->get_result()->fetch_assoc()["num"];
    Connection::$db->close();

    if($num == 0){
        Connection::connect();
        $query = "INSERT INTO Sceglie VALUES (?, ?)";

        $pq = Connection::$db->prepare($query);
        $pq->bind_param("ii", $id_utente, $id_pro);
        $res = $pq->execute();

        Connection::$db->close();

        if($res){
            echo "Prenotato con successo. Ritorna all'";
            echo "<a href='./index.php'>Home</a>";
        }else{
            echo "Prenotazione fallita. Ritorna all'";
            echo "<a href='./index.php'>Home</a>";
        }
    }else{
        echo "Programma già prenotato. Ritorna all'";
        echo "<a href='./index.php'>Home</a>";
    }
?>